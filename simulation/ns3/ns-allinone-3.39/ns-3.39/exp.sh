#!/bin/bash

exp_name="maxRandomInfo_updateInterval_push/1_0.1"
nNodes="30"
obsolescence="30"
directory_path="results/${exp_name}"
push="true"
pull="false"
updateInterval="0.1"
requestInterval="0.5"
broadcastInterval="1"
maxRandomNeighbors="1"
maxRandomInfo="1"
maxPos="200"

if [ ! -d "$directory_path" ]; then
    mkdir -p "$directory_path"
fi

./ns3 build
export NS_LOG=GossipSIRApplication='info|func|time|node|level'
./ns3 run gossip-experiment -- --nNodes=${nNodes} --outDir=${directory_path} --obsolescence=${obsolescence} --pull=${pull} --push=${push} --updateInterval=${updateInterval} --requestInterval=${requestInterval} --broadcastInterval=${broadcastInterval} --maxRandomNeighbors=${maxRandomNeighbors} --maxRandomInfo=${maxRandomInfo} --maxPos=${maxPos} > $directory_path/log.out 2>&1
echo -e "exp name:${exp_name}\nnNodes=${nNodes}\nobsolescence=${obsolescence}\npull=${pull}\npush=${push}\nupdateInterval=${updateInterval}\nrequestInterval=${requestInterval}" > $directory_path/param.txt

cd $directory_path
mergecap -w merged.pcap *.pcap
editcap -D 1000 --skip-radiotap-header merged.pcap final.pcap
capinfos -TmQ final.pcap > summary.csv
