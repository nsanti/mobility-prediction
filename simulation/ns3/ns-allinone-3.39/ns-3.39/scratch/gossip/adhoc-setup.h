/*
 * Copyright 2007 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef ADHOC_SETUP_H  
#define ADHOC_SETUP_H

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "ns3/boolean.h"

#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/ipv4-static-routing-helper.h"

namespace ns3
{

  class AdHocSetup
  {
    // verbose: turn on all WifiNetDevice log components
    //tracing:  turn on ascii and pcap tracing 
    public:

      /**
     * \brief Get the type ID.
     * \return the object TypeId
     */
      static TypeId GetTypeId();

      AdHocSetup();
      virtual ~AdHocSetup();

      NetDeviceContainer ConfigureDevices (NodeContainer &n, std::string phyMode, bool verbose, bool tracing);

      void SetOutDir(std::string out);

      std::string m_outDir;
  };

} // namespace ns3

#endif /* ADHOC_SETUP_H */
