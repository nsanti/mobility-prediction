/*
 * Copyright 2007 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "gossip-sir.h"

#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv6-address.h"
#include "ns3/log.h"
#include "ns3/nstime.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/socket.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/uinteger.h"
#include "ns3/boolean.h"
#include "ns3/wifi-module.h"

#include <random>

#define SUSCEPTIBLE 0
#define INFECTED 1
#define REMOVED 3

namespace ns3
{

NS_LOG_COMPONENT_DEFINE("GossipSIRApplication");

NS_OBJECT_ENSURE_REGISTERED(GossipSIR);

TypeId
GossipSIR::GetTypeId()
{
    static TypeId tid =
        TypeId("ns3::GossipSIRApplication")
            .SetParent<Application>()
            .SetGroupName("Applications")
            .AddConstructor<GossipSIR>()
            .AddAttribute(
                          "Obsolescence",
                          "Node's probabiliy to be removed after sending information (between 1 and 100)",
                          UintegerValue(100),
                          MakeUintegerAccessor(&GossipSIR::m_obsolescence),
                          MakeUintegerChecker<uint8_t>(1, 100))
            .AddAttribute("PushMode",
                          "If the node is in push mode",
                          BooleanValue(true),
                          MakeBooleanAccessor(&GossipSIR::m_push),
                          MakeBooleanChecker())
            .AddAttribute("PullMode",
                          "If the node is in pull mode",
                          BooleanValue(true),
                          MakeBooleanAccessor(&GossipSIR::m_pull),
                          MakeBooleanChecker())
            .AddAttribute("MaxRandomNeighbors",
                          "The maximum number of random neighbors selected to send update",
                          UintegerValue(1),
                          MakeUintegerAccessor(&GossipSIR::m_maxRandomNeighbors),
                          MakeUintegerChecker<uint8_t>())
            .AddAttribute("MaxRandomInfo",
                          "The maximum number of random infected info selected to send update",
                          UintegerValue(10),
                          MakeUintegerAccessor(&GossipSIR::m_maxRandomInfo),
                          MakeUintegerChecker<uint8_t>())
            .AddAttribute("SendUpdateInterval",
                          "Node's sending interval of information update",
                          TimeValue(Seconds(0.001)),
                          MakeTimeAccessor(&GossipSIR::m_updateInterval),
                          MakeTimeChecker()) 
            .AddAttribute("RequestInterval",
                          "Node's sending interval of request update",
                          TimeValue(Seconds(0.1)),
                          MakeTimeAccessor(&GossipSIR::m_requestInterval),
                          MakeTimeChecker())
            .AddAttribute("BroadcastInterval",
                          "Node's sending interval for discovering neighbors",
                          TimeValue(MilliSeconds(100)),
                          MakeTimeAccessor(&GossipSIR::m_broadcastInterval),
                          MakeTimeChecker());
    return tid;
}

GossipSIR::GossipSIR()
{
    NS_LOG_FUNCTION(this);

    m_updateEvent = EventId();
    m_removeEvent = EventId();
    m_requestEvent = EventId();
    m_broadcastEvent = EventId();

    m_maxRandomNeighbors = 1;
    m_maxRandomInfo = 1;

    m_broadcastInterval = MilliSeconds(100);
    m_removeNeighborsInterval = Seconds(1);

    m_state = INFECTED;
}

GossipSIR::~GossipSIR()
{
    NS_LOG_FUNCTION(this);

    // TODO
    // delete[] m_data;
    // m_data = nullptr;
    // m_dataSize = 0;
}

void
GossipSIR::DoDispose()
{
    NS_LOG_FUNCTION(this);
    Application::DoDispose();
}

void
GossipSIR::StartApplication()
{
    NS_LOG_FUNCTION(this);

    Ptr<Node> n = GetNode();

    GossipInformation myInfo;
    myInfo.count = rand() % 1000;
    myInfo.coverage = rand() % 100 + 1;
    myInfo.regionId = n->GetId();
    myInfo.state = INFECTED;
    m_infectedInfo.push_back(myInfo);
    m_gossipInfo.push_back(myInfo);

    for (uint32_t i = 0; i < n->GetNDevices(); i++)
    {

        Ptr<NetDevice> dev = n->GetDevice(i);
        if (dev->GetInstanceTypeId() == WifiNetDevice::GetTypeId())
        {
            m_wifiDevice = DynamicCast<WifiNetDevice> (dev);

            dev->SetReceiveCallback(MakeCallback(&GossipSIR::HandleRead, this));

            Ptr<WifiPhy> phy = m_wifiDevice->GetPhys()[0]; // There's only one PHY in a WaveNetDevice
            phy->TraceConnectWithoutContext("MonitorSnifferRx", MakeCallback(&GossipSIR::HandlePromiscRead, this));

            break;
        } 
    }

    if (m_wifiDevice)
    {
        // Randomness for broadcast packet time to avoid collision
        Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
        Time random_offset = MicroSeconds(rand->GetValue(50,200));
        m_broadcastInterval += random_offset;
        m_broadcastEvent = Simulator::Schedule(m_broadcastInterval, &GossipSIR::BroadCast, this);

    }
    else
    {
        NS_FATAL_ERROR ("There's no WifiNetDevice in your node");
    }

    m_removeEvent = Simulator::Schedule(m_removeNeighborsInterval, &GossipSIR::RemoveOldNeighbors, this); 

    if (m_push)
    {
        ScheduleUpdate(m_updateInterval);
    }
    if (m_pull)
    {
        ScheduleRequestUpdate(m_requestInterval);
    }
}

void
GossipSIR::StopApplication()
{
    NS_LOG_FUNCTION(this);

    if (m_wifiDevice)
    {
        m_wifiDevice->SetReceiveCallback(MakeNullCallback<bool, Ptr<NetDevice>, Ptr<const Packet>, uint16_t, const Address &>());
    }
    
    Simulator::Cancel(m_updateEvent);
    Simulator::Cancel(m_requestEvent);
    Simulator::Cancel(m_broadcastEvent);
    Simulator::Cancel(m_removeEvent);
}

std::string 
GossipSIR::GetNeighbors()
{
    std::ostringstream info; 

    info << "Neighbor Info for Node: " << GetNode()->GetId();

    for (std::vector<NeighborInformation>::iterator it = m_neighbors.begin(); it != m_neighbors.end(); it++)
    {
        std::cout << "\tMAC: " << it->neighborMac << "\tLast Contact: " << it->lastBeacon << std::endl;
    }

    return info.str();
}

bool
GossipSIR::GetRemovalProbability()
{
    uint8_t tmp_rnd;
    tmp_rnd = 1 + (rand() % m_obsolescence);
    return tmp_rnd < m_obsolescence;
}

std::vector<NeighborInformation>
GossipSIR::GetRandomNeighbors(uint8_t maxSelectedNeighbors)
{
    std::vector<NeighborInformation> selected;
    std::sample(m_neighbors.begin(), m_neighbors.end(), std::back_inserter(selected), 
                maxSelectedNeighbors,
                std::mt19937 {std::random_device{}()});
    return selected;
}

std::vector<GossipInformation>
GossipSIR::GetRandomInfos(uint8_t maxSelectedInfo)
{
    std::vector<GossipInformation> selected;
    std::sample(m_infectedInfo.begin(), m_infectedInfo.end(), std::back_inserter(selected), 
                maxSelectedInfo,
                std::mt19937 {std::random_device{}()});
    return selected;
}

void 
GossipSIR::InfosToBuffer(std::vector<GossipInformation> infos, uint8_t* buffer)
{
    NS_LOG_FUNCTION(this);
    NS_ASSERT_MSG(infos.size() < 500, "Too many infos to convert into a buffer (for packet sending)");

    size_t index = 0;  
    for (std::vector<GossipInformation>::iterator it = infos.begin(); it != infos.end(); it++, index += 4)
    {
        buffer[index]     = static_cast<uint8_t> (it->count & 0xFF); 
        buffer[index + 1] = static_cast<uint8_t> ((it->count >> 8) & 0xFF); 
        buffer[index + 2] = it->regionId; 
        buffer[index + 3] = it->coverage; 
    }
}

std::string
GossipSIR::GetInfos()
{
    return InfosToString(m_gossipInfo);
}

std::string
GossipSIR::InfosToString(std::vector<GossipInformation> infos)
{
    std::string res;
    for (std::vector<GossipInformation>::iterator it = infos.begin(); it != infos.end(); it++)
    {
        res += InfoToString(*it) + "\n";
    }
    return res;
}

std::string
GossipSIR::InfoToString(GossipInformation info)
{
    return "count : " + std::to_string(info.count) + 
    "; regionID : " + std::to_string(info.regionId) + 
    "; coverage : " + std::to_string(info.coverage) + 
    "; state : " + std::to_string(info.state);
}

void
GossipSIR::UpdateInfos(uint8_t* buffer, uint32_t size)
{
    NS_LOG_FUNCTION(this);
    for (uint32_t i = 0; i < size; i += 4) 
    {
        GossipInformation newGossip;

        newGossip.count = static_cast<uint16_t> (buffer[i]) | (static_cast<uint16_t> (buffer[i+1]) << 8);
        newGossip.regionId = buffer[i+2];
        newGossip.coverage = buffer[i+3];
        newGossip.state = SUSCEPTIBLE;

        std::vector<GossipInformation>::iterator it;
        it = std::find(m_gossipInfo.begin(), m_gossipInfo.end(), newGossip);

        if(it == m_gossipInfo.end())
        {
            NS_LOG_INFO("new gossip: " << InfoToString(newGossip) << "; nb of gossip : " << m_gossipInfo.size());
            // New info
            newGossip.state = INFECTED;
            m_gossipInfo.push_back(newGossip);
            m_infectedInfo.push_back(newGossip);
        }
        else if (it->state == INFECTED)
        {
            NS_LOG_INFO("already known gossip: " << InfoToString(newGossip) << "; nb of gossip : " << m_gossipInfo.size());
            // Already known info, chance to be removed
            if (GetRemovalProbability())
            {
                NS_LOG_INFO("gossip removed: " << InfoToString(newGossip) << "; nb of gossip : " << m_gossipInfo.size());
                it->state = REMOVED;
                newGossip.state = REMOVED;
                m_removedInfo.push_back(newGossip);

                auto it_infected = std::find(m_infectedInfo.begin(), m_infectedInfo.end(), newGossip);
                if(it_infected != m_infectedInfo.end()){m_infectedInfo.erase(it_infected);}
            }
        }
    } 
}

void
GossipSIR::ScheduleUpdate(Time dt)
{
    NS_LOG_FUNCTION(this << dt);
    m_updateEvent = Simulator::Schedule(dt, &GossipSIR::SendUpdate, this);
}

void
GossipSIR::ScheduleRequestUpdate(Time dt)
{
    NS_LOG_FUNCTION(this << dt);
    m_requestEvent = Simulator::Schedule(dt, &GossipSIR::SendRequestUpdate, this);
}

bool
GossipSIR::SendUpdate()
{
    NS_LOG_FUNCTION(this);

    std::vector<NeighborInformation> randomNeighbors;
    std::vector<GossipInformation> randomInfos;
    uint32_t bufferSize;
    std::string msg;
    bool res;

    res = true;

    randomNeighbors = GetRandomNeighbors(m_maxRandomNeighbors);
    randomInfos = GetRandomInfos(m_maxRandomInfo);

    if (!randomNeighbors.empty() && !randomInfos.empty())
    {
        bufferSize = 4 * randomInfos.size();
        uint8_t buffer[bufferSize];
        InfosToBuffer(randomInfos, buffer);

        for (std::vector<NeighborInformation>::iterator it = randomNeighbors.begin(); it != randomNeighbors.end(); it++)
        {
            res = res && Send(buffer, bufferSize, it->neighborMac);
        }
    }

    ScheduleUpdate(m_updateInterval);
    return res;
}

bool
GossipSIR::SendUpdateReply(Mac48Address dest)
{
    NS_LOG_FUNCTION(this);

    std::vector<GossipInformation> randomInfos;
    uint32_t bufferSize;
    std::string msg;

    randomInfos = GetRandomInfos(m_maxRandomInfo);

    if (!randomInfos.empty())
    {
        bufferSize = 4 * randomInfos.size();
        uint8_t buffer[bufferSize];
        InfosToBuffer(randomInfos, buffer);

        return Send(buffer, bufferSize, dest);
    } 
    
    return true;
}

bool
GossipSIR::SendRequestUpdate()
{
    NS_LOG_FUNCTION(this);

    std::vector<NeighborInformation> randomNeighbors;
    std::string msg;
    bool res;

    randomNeighbors = GetRandomNeighbors(m_maxRandomNeighbors);

    if (!randomNeighbors.empty())
    {
        msg = "REQUEST"; 
        res = true;

        for (std::vector<NeighborInformation>::iterator it = randomNeighbors.begin(); it != randomNeighbors.end(); it++)
        {
            res = res && Send(msg, it->neighborMac);
        }
    }
    ScheduleRequestUpdate(m_requestInterval);
    return res;
}

void 
GossipSIR::RemoveOldNeighbors()
{
    for (std::vector<NeighborInformation>::iterator it = m_neighbors.begin(); it != m_neighbors.end(); it++)
    {
        Time lastContact = Now() - it->lastBeacon;
        if (lastContact >= Seconds(5)) 
        {
            m_neighbors.erase (it);
            break;
        }    
    }

    m_removeEvent = Simulator::Schedule(m_removeNeighborsInterval, &GossipSIR::RemoveOldNeighbors, this); 
}

void
GossipSIR::UpdateState()
{
    // If the move was removed but a new information appeared
    if (m_state == REMOVED && !m_infectedInfo.empty())
    {
        m_state = INFECTED;
        ScheduleRequestUpdate(m_requestInterval);
    }
    // If the node was infected and received no new information, it has a chance to be removed
    else if (m_state == INFECTED && m_infectedInfo.empty() && GetRemovalProbability())
    {
        m_state = REMOVED;
        Simulator::Cancel(m_requestEvent);
    }
}

bool
GossipSIR::Send(std::string msg, Mac48Address dest)
{
    NS_LOG_FUNCTION(this);

    std::ostringstream msgx; 
    uint16_t packetSize;

    msgx << msg;
    packetSize = msgx.str().length();

    Ptr<Packet> packet = Create<Packet>((uint8_t*) msgx.str().c_str(), packetSize);
    
    return m_wifiDevice->Send(packet, dest, 0x8A);
}

bool
GossipSIR::Send(uint8_t* buffer, uint32_t size, Mac48Address dest)
{
    NS_LOG_FUNCTION(this);

    Ptr<Packet> packet = Create<Packet>(buffer, size);
    return m_wifiDevice->Send(packet, dest, 0x8A);
}

bool
GossipSIR::HandleRead(Ptr<NetDevice> device, Ptr<const Packet> packet, uint16_t protocol, const Address &sender)
{
    NS_LOG_FUNCTION(device << packet << protocol << sender);

    if (Mac48Address::IsMatchingType(sender))
    {
        std::ostringstream data;
        Mac48Address dest;

        dest = Mac48Address::ConvertFrom(sender);
        packet->CopyData(&data, packet->GetSize());

        NS_LOG_DEBUG("HandleRead() : data " << data.str());

        if (data.str() == "REQUEST")
        {
            return SendUpdateReply(dest);
        }
        else if (data.str() == "HELLO")
        {
            return true;
        }
        else
        {
            uint8_t buffer[packet->GetSize()];
            packet->CopyData(buffer, packet->GetSize());
            UpdateInfos(buffer, packet->GetSize());

            if (m_pull){UpdateState();}
            return true;
        }
        return true;
    }
    else 
    {
        NS_LOG_ERROR("HandleRead(): The address from the sender of the packet is not a MAC address");
        return false;
    }

}

void
GossipSIR::HandlePromiscRead(Ptr<const Packet> packet, uint16_t channelFreq, WifiTxVector tx, MpduInfo mpdu, SignalNoiseDbm sn, uint16_t sta_id)
{
    //This is a promiscous trace. It will pick broadcast packets, and packets not directed to this node's MAC address.
    /*
        Packets received here have MAC headers and payload.
        If packets are created with 1000 bytes payload, the size here is about 38 bytes larger. 
    */
    NS_LOG_DEBUG(Now() << " PromiscRx() : Node " << GetNode()->GetId() << " : ChannelFreq: " << channelFreq << " Mode: " << tx.GetMode()
                 << " Signal: " << sn.signal << " Noise: " << sn.noise << " Size: " << packet->GetSize()
                 << " Mode " << tx.GetMode()
                 );    

    Mac48Address destination;
    Mac48Address source;
    Mac48Address myMacAddress;
    WifiMacHeader hdr;

    if (packet->PeekHeader(hdr))
    {
        //Let's see if this packet is intended to this node
        destination = hdr.GetAddr1();
        source = hdr.GetAddr2();

        UpdateNeighbor(source);

        myMacAddress = m_wifiDevice->GetMac()->GetAddress();
        //A packet is intened to me if it targets my MAC address, or it's a broadcast message
        if (destination == Mac48Address::GetBroadcast() || destination == myMacAddress)
        {
            NS_LOG_DEBUG ("\tFrom: " << source << "\n\tSeq. No. " << hdr.GetSequenceNumber());
            //Do something for this type of packets
        }
        else //Well, this packet is not intended for me
        {
            //Maybe record some information about neighbors
        }    
    }
}

void
GossipSIR::BroadCast()
{
    NS_LOG_FUNCTION (this);

    std::string msg;
    msg = "HELLO";
    Send(msg, Mac48Address::GetBroadcast());

    // Schedule next broadcast 
    Simulator::Cancel(m_broadcastEvent);
    m_broadcastEvent = Simulator::Schedule (m_broadcastInterval, &GossipSIR::BroadCast, this);

}

void 
GossipSIR::UpdateNeighbor(Mac48Address addr)
{
    bool found;
    found = false;

    for (std::vector<NeighborInformation>::iterator it = m_neighbors.begin(); it != m_neighbors.end(); it++)
    {
        if (it->neighborMac == addr)
        {
            it->lastBeacon = Now();
            found = true;
            break;
        }
    }
    if (!found) 
    {
        NeighborInformation newNeighbor;
        newNeighbor.neighborMac = addr;
        newNeighbor.lastBeacon = Now();
        m_neighbors.push_back(newNeighbor);
    }
}

} // Namespace ns3
