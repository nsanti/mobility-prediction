/*
 * Copyright 2007 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef GOSSIP_SIR_H
#define GOSSIP_SIR_H

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ipv4-address.h"
#include "ns3/ptr.h"
#include "ns3/traced-callback.h"
#include "ns3/wifi-net-device.h"
#include "ns3/wifi-phy.h"

namespace ns3
{

class Socket;
class Packet;

/** \brief A struct to represent information about this node's neighbors. 
*/
struct NeighborInformation
    {
        Mac48Address neighborMac;
        Time lastBeacon;

        bool operator==(const NeighborInformation& rhs) const
        {
          return neighborMac == rhs.neighborMac;
        }
    };

/** \brief A struct to represent a gossip information. 
*/
struct GossipInformation
    {
        uint16_t count;
        uint8_t regionId;
        uint8_t coverage;
        uint8_t state;

        bool operator==(const GossipInformation& rhs) const
        {
          return count == rhs.count &&
                 regionId == rhs.regionId &&
                 coverage == rhs.coverage;
        }
    };

/**
 * \ingroup gossipsir 
 * \brief A gossip SIR algorithm 
 */
class GossipSIR: public Application
{
  public:
    /**
     * \brief Get the type ID.
     * \return the object TypeId
     */
    static TypeId GetTypeId();

    GossipSIR();

    ~GossipSIR() override;
  
    /**
     * \brief The neighbor information of this node
     * \return The string representing the neighbors of this node
    */
    std::string GetNeighbors();

    /**
     * \brief The gossip information detained by this node.
     * \return The string representing the gossip info.
    */
    std::string GetInfos();

  protected:
    void DoDispose() override;

  private:
    void StartApplication() override;
    void StopApplication() override;
    
    /**
     * \brief If a node is to be removed.
     * \return A true boolean if the node is to be removed, false boolean else.
    */
    bool GetRemovalProbability();

    /**
     * \brief Select some of the node's neighbors.
     * \param maxSelectedNeighbors The maximum of selected neighbors.
     * \return The information of the selected neighbors.
    */
    std::vector<NeighborInformation> GetRandomNeighbors(uint8_t maxSelectedNeighbors);

    /**
     * \brief Select some of the node's informations.
     * \param maxSelectedInfo The maximum of selected informations.
     * \return The information of the selected informations.
    */
    std::vector<GossipInformation> GetRandomInfos(uint8_t maxSelectedInfos);

    /**
     * \brief Transform a gossip information into a ready to send buffer.
     * \param infos The gossip informations to transform, vector size must be under 500.
     * \param buffer The buffer where to put the gossip information.
    */
    void InfosToBuffer(std::vector<GossipInformation> infos, uint8_t* buffer);

    /**
     * \brief Transform a vector of gossip information into a string representation.
     * \param infos The vector of gossip information to transform.
     * \return The string representation.
    */
    std::string InfosToString(std::vector<GossipInformation> infos);

    /**
     * \brief Transform a gossip into a string representation.
     * \param info The gossip information to transform.
     * \return The string representation.
    */
    std::string InfoToString(GossipInformation info);

    /**
     * \brief Handle a buffer into gossip information in the node.
     * \param buffer The buffer contening the information to transform (the buffer's size must be >3).
     * \param size The size of the buffer, must be > 3.
    */
    void UpdateInfos(uint8_t* buffer, uint32_t size);

    /**
     * \brief In the pull mode, decide the state of the node after receiving informations.
    */
    void UpdateState();

    /**
     * \brief Update the neigbors when receiving a promiscuous packet
     * \param addr The MAC adress of the neighbor to add
    */
    void UpdateNeighbor(Mac48Address addr);

    /**
     * \brief Remove old neighbors 
    */
    void RemoveOldNeighbors();

    /**
     * \brief Send a packet to the MAC destination.
     * \param msg The string message to send.
     * \param dest The MAC address of the destination.
     * \return A true boolean if the send operation is successful, false otherwise.
    */
    bool Send(std::string msg, Mac48Address dest);

    /**
     * \brief Send a packet to the MAC destination.
     * \param buffer The buffer to send.
     * \param size The size of the buffer to send.
     * \param dest The MAC address of the destination.
     * \return A true boolean if the send operation is successful, false otherwise.
    */
    bool Send(uint8_t* buffer, uint32_t size, Mac48Address dest);

    /**
     * \brief Send the gossip update to random neighbors.
     * \return A true boolean if the send operation is successful, false otherwise.
    */
    bool SendUpdate();

    /**
     * \brief Send the gossip update to a requesting neighbor.
     * \param dest The destination MAC address.
     * \return A true boolean if the send operation is successful, false otherwise.
    */
    bool SendUpdateReply(Mac48Address dest);

    /**
     * \brief Send the gossip request update
    */
    bool SendRequestUpdate();

    /**
     * \brief Schedule the gossip update
     * \param dt The update inter-send time
    */
    void ScheduleUpdate(Time dt);

    /**
     * \brief Schedule the gossip request update
     * \param dt The request update inter-send time
    */
    void ScheduleRequestUpdate(Time dt);

    /**
     * \brief Broadcast a packet for neighbors to discover the node
    */
    void BroadCast();

    /**
     * \brief Handle a packet reception.
     * \param device The device that receive a packet.
     * \param packet The received packet.
     * \param protocol The protocol associated with the packet.
     * \param sender The sender's address.
     * \return A true boolean if the packet was handled successfully.
     * 
    */
    bool HandleRead(Ptr<NetDevice> device, Ptr<const Packet> packet, uint16_t protocol, const Address &sender);

    /**
     * \brief Handle a promiscuous packet reception.
     * \param packet The promiscuous received packet.
     * \param channelFreq The channel's frequency.
     * \param tx The transmission's parameters.
     * \param mpdu The MAC Protocol Data Unit.
     * \param sn The signal noise.
     * \param sta_id The station's id.
     * 
    */
    void HandlePromiscRead
    (
      Ptr<const Packet> packet, uint16_t channelFreq, WifiTxVector tx, MpduInfo mpdu, SignalNoiseDbm sn, uint16_t sta_id
    );

    uint8_t m_state; //!< Application's state (either SUSCEPTIBLE, INFECTED, REMOVED)
    uint8_t m_obsolescence; //!< Node's probabiliy to be removed after sending information (between 1 and 100)
    uint8_t m_maxRandomNeighbors; //!< The maximum number of random neighbors selected to send update
    uint8_t m_maxRandomInfo; //!< The maximum number of random infected info selected to send update

    bool m_push; //!< If application is in push mode 
    bool m_pull; //!< If application is in pull mode 

    Time m_updateInterval;  //!< Update packet inter-send time
    Time m_requestInterval;  //!< Update request packet inter-send time
    Time m_broadcastInterval;  //!< Neighbours broadcast packet inter-send time
    Time m_removeNeighborsInterval;  //!< Removal of old neighbors inter time

    EventId m_updateEvent; //!< Id of the update event
    EventId m_requestEvent; //!< Id of the request event
    EventId m_removeEvent; //!< Id of the neighbors' removal event
    EventId m_broadcastEvent; //!< Id of the broadcast event

    Ptr<WifiNetDevice> m_wifiDevice; //!< A WaveNetDevice that is attached to this device 

    std::vector<NeighborInformation> m_neighbors; //!< A vector representing neighbors of this node 
    std::vector<GossipInformation> m_infectedInfo; //!< A vector representing infected information from the gossip of this node 
    std::vector<GossipInformation> m_removedInfo; //!< A vector representing removed (old) information from the gossip of this node 
    std::vector<GossipInformation> m_gossipInfo; //!< A vector representing information from the gossip of this node 
};

} // namespace ns3

#endif /* GOSSIP_SIR_H */
