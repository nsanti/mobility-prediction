/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "adhoc-setup.h"
#include "gossip-sir.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("GossipExperiment");

int
main(int argc, char* argv[])
{

    uint8_t nNodes = 2; 
    uint8_t obsolescence = 30; 
    uint8_t maxRandomNeighbors = 1; 
    uint8_t maxRandomInfo = 1; 
    bool push = true; 
    bool pull = true; 
    Time updateInterval = Seconds(1);  
    Time requestInterval = Seconds(2); 
    Time broadcastInterval = Seconds(3);
    int nodeSpeed = 15; // in m/s
    int nodePause = 5;  // in s
    int maxPos = 5;  // in m
    std::string outDir = "";

    CommandLine cmd(__FILE__);
    cmd.AddValue("outDir", "The name of the file where we will stock the results", outDir);
    cmd.AddValue("nNodes", "Number of nodes", nNodes);
    cmd.AddValue("obsolescence", "Node's probabiliy to be removed after sending information (between 1 and 100)", obsolescence);
    cmd.AddValue("push", "If gossip application is in push mode", push);
    cmd.AddValue("pull", "If gossip application is in pull mode", pull);
    cmd.AddValue("updateInterval", "Update packet inter-send time", updateInterval);
    cmd.AddValue("requestInterval", "Update request packet inter-send time", requestInterval);
    cmd.AddValue("broadcastInterval", "Neighbours broadcast packet inter-send time", broadcastInterval);
    cmd.AddValue("maxRandomNeighbors", "Maximum number of neighbors to send gossip to", maxRandomNeighbors);
    cmd.AddValue("maxRandomInfo", "Maximum number of gossid to send", maxRandomInfo);
    cmd.AddValue("maxPos", "The X and Y boundaries of the map", maxPos);
    cmd.Parse(argc, argv);

    Time::SetResolution(Time::NS);
    LogComponentEnable("GossipSIRApplication", LOG_LEVEL_INFO);
    LogComponentEnable("GossipExperiment", LOG_LEVEL_INFO);
    LogComponentEnable("AdHocSetup", LOG_LEVEL_INFO);

    NodeContainer nodes;
    nodes.Create(nNodes);

    MobilityHelper mobilityAdhoc;
    int64_t streamIndex = 0; // used to get consistent mobility across scenarios

    ObjectFactory pos;
    pos.SetTypeId("ns3::RandomRectanglePositionAllocator");
    std::stringstream ssPos;
    ssPos << "ns3::UniformRandomVariable[Min=0.0|Max=" << maxPos << "]";
    pos.Set("X", StringValue(ssPos.str()));
    pos.Set("Y", StringValue(ssPos.str()));

    Ptr<PositionAllocator> taPositionAlloc = pos.Create()->GetObject<PositionAllocator>();
    streamIndex += taPositionAlloc->AssignStreams(streamIndex);

    std::stringstream ssSpeed;
    ssSpeed << "ns3::UniformRandomVariable[Min=0.0|Max=" << nodeSpeed << "]";
    std::stringstream ssPause;
    ssPause << "ns3::ConstantRandomVariable[Constant=" << nodePause << "]";
    mobilityAdhoc.SetMobilityModel("ns3::RandomWaypointMobilityModel",
                                   "Speed",
                                   StringValue(ssSpeed.str()),
                                   "Pause",
                                   StringValue(ssPause.str()),
                                   "PositionAllocator",
                                   PointerValue(taPositionAlloc));
    mobilityAdhoc.SetPositionAllocator(taPositionAlloc);
    mobilityAdhoc.Install(nodes);

    AdHocSetup adhoc;
    NetDeviceContainer devices;
    adhoc.SetOutDir(outDir);
    devices = adhoc.ConfigureDevices(nodes, "CckRate11Mbps", false, true);

    for (uint32_t i = 0 ; i < nodes.GetN() ; i++)
    {
        ObjectFactory fact;
        Ptr<GossipSIR> appI;

        fact.SetTypeId("ns3::GossipSIRApplication");
        fact.Set("Obsolescence", UintegerValue(obsolescence));
        fact.Set("PushMode", BooleanValue(push));
        fact.Set("PullMode", BooleanValue(pull));
        fact.Set("SendUpdateInterval", TimeValue(updateInterval));
        fact.Set("RequestInterval", TimeValue(requestInterval));
        fact.Set("BroadcastInterval", TimeValue(broadcastInterval));
        fact.Set("MaxRandomInfo", UintegerValue(maxRandomInfo));
        fact.Set("MaxRandomNeighbors", UintegerValue(maxRandomNeighbors));
        
        appI = fact.Create<GossipSIR>();
        appI->SetStartTime(Seconds(1));
        appI->SetStopTime(Seconds(60));
        nodes.Get(i)->AddApplication(appI);
    }

    Simulator::Stop(Seconds(65));
    Simulator::Run();

    /*
    for (uint32_t i=0 ; i<nodes.GetN(); i++)
    {
        Ptr<GossipSIR> appI = DynamicCast<GossipSIR> (nodes.Get(i)->GetApplication(0));
        std::cout << appI->GetNeighbors() << std::endl;
        std::cout << appI->GetInfos() << std::endl;
    }
    */

    Simulator::Destroy();
    return 0;
}
