/*
 * Copyright 2007 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "adhoc-setup.h"

namespace ns3
{

NS_LOG_COMPONENT_DEFINE("AdHocSetup");

NS_OBJECT_ENSURE_REGISTERED(AdHocSetup);

TypeId
AdHocSetup::GetTypeId()
{
    static TypeId tid =
        TypeId("ns3::AdHocSetup");
    return tid;
}

AdHocSetup::AdHocSetup()
{
    m_outDir == ".";
}
AdHocSetup::~AdHocSetup(){}

void 
AdHocSetup::SetOutDir(std::string out)
{
    m_outDir = out;
}

NetDeviceContainer AdHocSetup::ConfigureDevices (NodeContainer& nodes, std::string phyMode, bool verbose, bool tracing)
{

    // Fix non-unicast data rate to be the same as that of unicast
    //Config::SetDefault("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue(phyMode));

    // Fix non-unicast data rate to be the same as that of unicast
    //Config::SetDefault("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue(phyMode));

    WifiHelper wifi;
    if (verbose)
    {
        WifiHelper::EnableLogComponents(); // Turn on all Wifi logging
    }

    YansWifiPhyHelper wifiPhy;
    // set it to zero; otherwise, gain will be added
    wifiPhy.Set("RxGain", DoubleValue(-10));
    wifiPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);

    YansWifiChannelHelper wifiChannel;
    wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
    wifiChannel.AddPropagationLoss("ns3::NakagamiPropagationLossModel");
    wifiChannel.AddPropagationLoss("ns3::FriisPropagationLossModel");
    //wifiChannel.AddPropagationLoss("ns3::TwoRayGroundPropagationLossModel");
    wifiPhy.SetChannel(wifiChannel.Create());

    // Add an upper mac and disable rate control
    WifiMacHelper wifiMac;
    wifi.SetStandard(WIFI_STANDARD_80211n);
    /*
    wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                 "DataMode",
                                 StringValue(phyMode),
                                 "ControlMode",
                                 StringValue(phyMode));
    */

    // Set it to adhoc mode
    wifiMac.SetType("ns3::AdhocWifiMac");
    NetDeviceContainer devices = wifi.Install(wifiPhy, wifiMac, nodes);

    if (tracing)
    {
        AsciiTraceHelper ascii;
        wifiPhy.EnableAsciiAll(ascii.CreateFileStream(m_outDir + "/adhoc-setup.tr"));
        wifiPhy.EnablePcap(m_outDir + "/adhoc-setup", devices);
    }

  return devices;
}

} // Namespace ns3
