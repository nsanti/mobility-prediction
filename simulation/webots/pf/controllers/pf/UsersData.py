import read_geolife as read_geolife
import numpy as np
import pandas as pd

# abstract
class UsersData:
    
    def __init__(self, data_path):
        
        self.df_users  = []
        self.data_path = data_path

        self.__init_users()
        
    def __init_users(self):
        
        self.df_users = pd.read_csv(self.data_path + '/data.csv')
        self.df_users['time'] = pd.to_datetime(self.df_users['time'])

        self.df_users = self.df_users.sort_values(by=['time'])

    def get_users(self):
    
        users     = []
        users_ids = self.df_users.user.unique()

        users_homes = pd.read_csv(self.data_path + '/homes.csv')
        
        for user_id in users_ids:
            
            df_user = self.df_users[self.df_users.user == user_id]

            home_lon = users_homes.loc[users_homes['user'] == user_id, 'lon'].values[0]
            home_lat = users_homes.loc[users_homes['user'] == user_id, 'lat'].values[0]
            
            departure_time = df_user['time'].min()
            lon, lat = df_user.loc[df_user['time'] == departure_time, ['lon', 'lat']].iloc[0]
            
            # user : [id, position, home, departure_time]
            users.append([str(user_id), (lon, lat), (home_lon, home_lat), departure_time])
    
        return users
    
    def get_user_nearest_observation_position(self, particle, delta_time):
    
        df_user = self.df_users[(self.df_users['user'] == particle.id)]
        
        time    = particle.departure_time + pd.Timedelta(seconds=delta_time)
        idx     = np.searchsorted(df_user['time'], time)      
        
        if idx >= df_user.shape[0]:
            # no more data for this user
            return None
              
        else :
            # return the nearest time and the position associated
            nearest_time = df_user.iloc[idx].time
            
            # to have the difference 
            difference = (nearest_time - time).total_seconds()
            
            return (df_user.iloc[idx].lon, df_user.iloc[idx].lat), nearest_time, difference
        
    def get_nb_people_regions(self, particles, simulator, delta_time, time_window=30):
        
        n_rows = int((simulator.ymax - simulator.ymin) / simulator.regions_size) + 1
        n_cols = int((simulator.xmax - simulator.xmin) / simulator.regions_size) + 1
        nb_people_regions = np.zeros((n_rows, n_cols))
        
        for p in particles :
            
            # in case there is no position information for the exact timestamp
            start_time = p.departure_time + pd.Timedelta(seconds=delta_time)
            end_time   = start_time + pd.Timedelta(seconds=time_window)
            
            user_data = self.df_users.loc[self.df_users['user'] == int(p.origin_id)]
            in_window = user_data.loc[(self.df_users['time'] >= start_time) & (self.df_users['time'] <= end_time)]

            if not in_window.empty :
                
                line = in_window.loc[in_window['time'] == in_window['time'].min()]

                lon = line.iloc[0]['lon']
                lat = line.iloc[0]['lat']
                
                x, y = simulator.convert_LonLat_to_XY(lon, lat)
                x_idx = int((x - simulator.xmin) // simulator.regions_size)
                y_idx = int((y - simulator.ymin) // simulator.regions_size)
        
                nb_people_regions[y_idx][x_idx] += 1

        return nb_people_regions 