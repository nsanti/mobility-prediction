"""pf controller."""

import os
import ast
import sys
import time 
import struct

from UsersData import UsersData
from ParticleFilter import ParticleFilter
from controller import Robot, Receiver, Emitter

# robot init
robot = Robot()
timestep = int(robot.getBasicTimeStep())

# receiver and emitter init
receiver = robot.getDevice("receiver") 
receiver.enable(timestep)

emitter = robot.getDevice("emitter")

# get args
robot_id = sys.argv[1]
skip_obs_steps = ast.literal_eval(sys.argv[2])

# seed for synchronisation of the random generator
seed = 1688813763

# pf function init
output_dir = f'../../results/exp_1/robot_{robot_id}'
output_file = output_dir + '/output.txt'

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

if os.path.isfile(output_file):
    os.remove(output_file)
open(output_file, 'w').close()

users_data = UsersData('data/200_10')           

sim_time = 10 # seconds
obs_time = 20 # seconds
end_time = 600 # seconds 

region_size                           = 250 
obs_confidence                        = 0.9
position_noise                        = 0.1
behavorial_noise                      = 0.01
user_allowance_ratio                  = 1
operator_diffusion_ratio              = 1 
probability_observation_nearby_people = 0.1

transition_probalities_model = [[0.05,0.1,0.15,0.2], [0,0.05,0.1,0.15], [0.05,0.1], [0,0.1,0.2,0.3]]

pf = ParticleFilter(
                    region_size, sim_time, obs_confidence, behavorial_noise, position_noise, \
                    operator_diffusion_ratio, user_allowance_ratio, probability_observation_nearby_people, \
                    transition_probalities_model,
                    seed, output_dir\
                   )

pf.initialize_particles(users_data.get_users())
pf.initialize_simulator("sumo_files/map.sumocfg", "sumo_files/net.xml", "/usr/bin/sumo")

obs_delta = 0
end_delta = 0
obs_steps = 0
previous_time = time.time()

while robot.step(timestep) != -1:

    actual_time = time.time()

    if actual_time - previous_time >= sim_time:

        if end_delta >= end_time: # end of the simulation

            break

        elif (obs_delta >= obs_time) and (obs_steps not in skip_obs_steps): # observation step

            obs_data = users_data.get_nb_people_regions(pf.particles, pf.simulator, pf.delta_time)
            pf.observation_step(obs_data)
            obs_delta = 0
            obs_steps += 1

        else: # simulation step

            pf.blind_step()
            obs_delta += sim_time
        
        end_delta += sim_time
        previous_time = actual_time

sys.exit(0)

