import numpy as np
from pyemd import emd, emd_with_flow

import copy 
import plot as plot

from Particle import Particle
from UsersData import UsersData
from TransitionModel import TransitionModel
from Simulator import HumanMobilitySimulator

import os, sys
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:   
    sys.exit("please declare environment variable 'SUMO_HOME'")
    
import sumolib
import traci


class ParticleFilter:
    
    def __init__(self, region_size, time_step_seconds,\
                obs_confidence, behavorial_noise, position_noise, operator_diffusion_ratio, \
                user_allowance_ratio, probability_observation_nearby_people, \
                transition_probabilities_model, \
                seed, output_dir=None):

        self.regions_size = region_size

        self.time_step_seconds    = time_step_seconds 
        self.observation_interval = 3 

        self.obs_confidence   = obs_confidence
        self.position_noise   = position_noise 
        self.behavorial_noise = behavorial_noise 

        self.user_allowance_ratio                  = user_allowance_ratio    
        self.operator_diffusion_ratio              = operator_diffusion_ratio 
        self.probability_observation_nearby_people = probability_observation_nearby_people   

        self.transition_probabilities_model = transition_probabilities_model

        self.transition_model = TransitionModel()

        self.time_step  = 1  
        self.delta_time = 0

        self.output_dir = output_dir 

        self.generator = np.random.default_rng(seed)

    def initialize_particles(self, users_init_data):
        # [weight, state]
        # state = [(lat,long), [transition proba]]
        
        particles           = []
        
        number_of_particles = len(users_init_data) 
        weight              = 1/number_of_particles
        
        for user in users_init_data:
            
            # user : [id, position, home, departure_time]
            particle = Particle(user[0], self.transition_probabilities_model, weight, user[1], user[2], user[3], vehicle_type="my_pedestrian")
            particles.append(particle)
        
        self.particles = particles
        self.number_of_particles = number_of_particles

    def initialize_simulator(self, sumo_config, sumo_net, sumo_binary):

        self.simulator = HumanMobilitySimulator(sumo_config, sumo_net, sumo_binary, \
                                           self.regions_size, self.particles, output_file=self.output_dir + '/output.txt')

        self.clean_particles()

    def clean_particles(self):
        
        self.particles = [particle for particle in self.particles if particle.loaded]
        self.number_of_particles = len(self.particles)

    def compute_likelihood(self, particle, nb_people_regions, k, u, l):

        likelihood  = 1.0  # initialize likelihood
        
        (x, y) = self.simulator.get_cartesian_position(particle)

        col = int((x - self.simulator.xmin) // self.regions_size)
        row = int((y - self.simulator.ymin) // self.regions_size)
        
        nb_people_region = nb_people_regions[row][col]
        correct_obs = self.generator.binomial(nb_people_region, k * u * l)

        adj_regions = self.simulator.get_adjacent_regions(nb_people_regions, row, col)
        
        for adj_row, adj_col in adj_regions :
    
            nb_people_adj_region = nb_people_regions[adj_row][adj_col]
        
            wrong_obs = self.generator.binomial(nb_people_adj_region, k * u * (1 - l))

            self.output([f'    - likelihood formula : {likelihood} * ({k} * {u} * ({correct_obs} + {wrong_obs}))\n'])

            likelihood *= (k * u * (correct_obs + wrong_obs))
        
        return likelihood
            
    def resample(self, samples):
        
        # Get list with only weights
        weights = [sample.weight for sample in samples]
        
        Q = np.cumsum(weights).tolist()
        
        n = 0
        new_particles = []
        while n < len(samples):

            # Draw a random sample u
            u = self.generator.uniform(1e-6, 1, 1)[0]

            # Naive search
            m = 0
            try: 

                while Q[m] < u:
                    m += 1

            except IndexError as err :
                self.output([f"No weight over the random sample: {u}\n"])
                continue

            # Add copy of the particle but set uniform weight
            new_sample = copy.deepcopy(samples[m])
            new_sample.weight = 1.0/len(samples)
            new_sample.id += f'#{n}'
            new_particles.append(new_sample)

            # Added another sample
            n += 1

        return new_particles 

    def normalize_weights(self, samples):
            
            new_samples = []

            sum_weights = 0.0
            for sample in samples:
                sum_weights += sample.weight 
            
            if sum_weights < 1e-15:

                self.output([f"Weight normalization failed, sum of weights: {sum_weights} (weights will be reinitialized)\n"])

                for sample in samples:
                    new_sample = copy.deepcopy(sample)
                    new_sample.weight = 1 / len(samples)
                    self.output([f"Reinitialized weight for particle {new_sample.id}: {new_sample.weight}\n"])
                    new_samples.append(new_sample)

            else :

                self.output([f"Weight normalization, sum of weights: {sum_weights}\n"])

                for sample in samples:
                    new_sample = copy.deepcopy(sample)
                    new_sample.weight /= sum_weights 
                    self.output([f"Weight for particle {new_sample.id}: {new_sample.weight}\n"])
                    new_samples.append(new_sample)
            
            return new_samples

    def output(self, lines):

        if self.output_dir:
            with open(self.output_dir + '/output.txt', "a+") as f:
                for l in lines:
                    f.write(l)

    def blind_step(self):

        err_particles = []
        for i, particle in enumerate(self.particles):
        
            self.output([
                        f"[{i+1}/{len(self.particles)}] particle {particle.id}:\n", \
                        f"\n    {particle}\n", \
                        f"    - old destination: {particle.destination}\n"
                        ])
            
            # get the new destination
            self.simulator.update_state(particle)
            particle.destination = self.transition_model.get_new_destination(particle.destination, particle.transition_probabilities)
            self.output([f"    - new destination: {particle.destination}\n"])
            
            try:
                # update in the simulator
                self.simulator.set_new_destinations(particle)      
            except RuntimeError as err:
                self.output([f'     Handling destination error for particle {particle.id}: {err}\n'])
                err_particles.append(particle)
                self.simulator.remove_user(particle)
        self.particles      = list(set(self.particles).difference(err_particles)) # remove particles with errors
        number_of_particles = len(self.particles)
    
        self.output([f"Simulation step of {self.time_step_seconds}\n"])

        self.simulator.simulation_step(self.time_step_seconds) # move in simulator
    
        self.output([f"Update position:\n"])

        for i, particle in enumerate(self.particles):
        
            self.output([
                        f"[{i+1}/{len(self.particles)}] particle {particle.id}:\n", \
                        f"    - old position: {particle.position}\n", \
                        ])
            particle.position = self.simulator.get_gps_position(particle) 
            self.output([f"    - new position: {particle.position}\n"])
        
        self.time_step  += 1
        self.delta_time += self.time_step_seconds
    
    def observation_step(self, obs_data):

        self.output([f"Observation provided, update:\n"])  
        sim_data = self.simulator.get_nb_people_in_region(self.particles, self.regions_size)

        if self.output_dir:
            xmin = self.simulator.xmin
            xmax = self.simulator.xmax
            ymin = self.simulator.ymin
            ymax = self.simulator.ymax
            regions_size = self.simulator.regions_size
            plot.plot_density(sim_data, f'{self.output_dir}/sim_{self.time_step}')
            plot.plot_density(obs_data, f'{self.output_dir}/obs_{self.time_step}')
            self.output([f"sim data {self.time_step} : {sim_data}\n"]);
            self.output([f"obs data {self.time_step} : {obs_data}\n"]);

        first_histogram  = np.array(sim_data, dtype=np.float64)
        second_histogram = np.array(obs_data, dtype=np.float64)

        value, flow = emd_with_flow(first_histogram.flatten(), second_histogram.flatten(), self.simulator.distance_matrix)

        sum_likelihood_factor = 0
        err_particles = []
        for i, particle in enumerate(self.particles):
            
            self.output([
                        f"[{i+1}/{len(self.particles)}] particle {particle.id}:\n", \
                        f"    - old simulator position  : {particle.position}\n", \
                        ])
            
            (x, y) = self.simulator.get_cartesian_position(particle)
            col    = int((x - self.simulator.xmin) // self.regions_size)
            row    = int((y - self.simulator.ymin) // self.regions_size) 
            n_cols = int((self.simulator.xmax - self.simulator.xmin) // self.simulator.regions_size + 1)
            origin = row * n_cols + col

            (x_o, y_o) = self.simulator.regions_central_point[row][col]

            condition   = lambda j, d: d < 0  and j != origin
            destination = next((j for j, d in enumerate(flow[origin]) if condition(j, d)), None) # search the first edm flow

            if destination is not None:

                    self.output([f"    - approach particle from region {origin} to region {destination}\n"])

                    (x_d, y_d) = self.simulator.regions_central_point.flatten()[destination]

                    x = (1 - self.obs_confidence) * x_o + self.obs_confidence * x_d
                    y = (1 - self.obs_confidence) * y_o + self.obs_confidence * y_d

                    try :
                        self.simulator.move_userXY(particle, x, y, self.time_step_seconds, threshold=250)
                    except RuntimeError as err:
                        self.output([f'     Handling moving error for particle {particle.id}: {err}\n'])
                        err_particles.append(particle)
                        self.simulator.remove_user(particle)
                        continue

                    flow[origin][destination] -= 1

            lon, lat = self.simulator.get_gps_position(particle)
            particle.position = (lon, lat)

            self.output([
                        f"    - new observation position: {particle.position}\n", \
                        f"    - old transition proba: {particle.transition_probabilities}\n", \
                        ])

            particle.transition_probabilities = list(map(lambda x: x + self.behavorial_noise, \
                                                    particle.transition_probabilities))
            self.output([f"    - new transition proba: {particle.transition_probabilities}\n"])
            
            particle.likelihood = self.compute_likelihood(particle, obs_data, \
                                                      self.operator_diffusion_ratio, self.user_allowance_ratio, \
                                                      self.probability_observation_nearby_people) 
            self.output([f"    - likelihood : {particle.likelihood}n"])

            particle.weight = particle.likelihood
            self.output([f"    - weight : {particle.weight}\n"])

        self.particles      = list(set(self.particles).difference(err_particles)) # remove particles with errors
        number_of_particles = len(self.particles)

        self.particles = self.normalize_weights(self.particles) 
        new_particles  = self.resample(self.particles) 

        self.simulator.reload_users(self.particles, new_particles, self.time_step_seconds)
        self.particles = new_particles

        self.time_step  += 1
        self.delta_time += self.time_step_seconds