import copy
import os, sys
import numpy as np
from scipy.spatial.distance import cdist
from TransitionModel import Destinations

if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:   
    sys.exit("please declare environment variable 'SUMO_HOME'")
    
import sumolib
import traci

# abstract (not all the class)
class HumanMobilitySimulator:
    
    def __init__(self, sumo_config_file, sumo_net_file, sumo_binary_path, regions_size, users, time_step_seconds=30, output_file=None):
    
        self.adjacent_regions = []
        self.regions          = []
        self.regions_size     = regions_size

        self.distance_matrix       = []
        self.regions_central_point = []

        self.output_file = output_file

        self.net = sumolib.net.readNet(sumo_net_file)
 
        self.__init_sumo(sumo_binary_path, sumo_config_file)
    
        (xmin, ymin), (xmax, ymax) = traci.simulation.getNetBoundary()
        
        self.xmin = int(xmin)
        self.xmax = int(xmax)
        self.ymin = int(ymin)
        self.ymax = int(ymax)
    
        self.__init_regions()
        self.__init_distance_matrix()
        
        self.__init_users(users, time_step_seconds)

    def __init_regions(self): 

        self.output([
                    f"[Simulator] Init regions\n", \
                    f"    - Regions size : {self.regions_size}\n", \
                    f"    - Map min : ({self.xmin},{self.ymin})\n", \
                    f"    - Map max : ({self.xmax},{self.ymax})\n", \
                    ])

        
        '''
        for x in range(self.xmin, self.xmax, self.regions_size) :
            for y in range(self.ymin, self.ymax, self.regions_size) :
                self.regions.append((x, y, x+self.regions_size, y+self.regions_size))
        '''
        n_rows = int((self.ymax - self.ymin) / self.regions_size) + 1
        n_cols = int((self.xmax - self.xmin) / self.regions_size) + 1
        self.regions = np.zeros((n_rows, n_cols))

        self.output([f"    - Number of regions : {self.regions.size}\n"])

    def __init_distance_matrix(self):

        cols = (self.xmax - self.xmin) // self.regions_size + 1
        rows = (self.ymax - self.ymin) // self.regions_size + 1

        x_centers = np.linspace(self.xmin + self.regions_size/2, self.xmax - self.regions_size/2, cols)
        y_centers = np.linspace(self.ymin + self.regions_size/2, self.ymax - self.regions_size/2, rows)
        
        xv, yv = np.meshgrid(x_centers, y_centers)
        centers = np.column_stack((xv.ravel(), yv.ravel()))

        self.distance_matrix = cdist(centers, centers, metric='euclidean')
        self.regions_central_point = centers.reshape(rows, cols, 2)
            
    def __init_sumo(self, sumo_binary_path, sumo_config_file):
        
        if traci.isLoaded():
            traci.close()
        
        traci.start([sumo_binary_path, "-c", sumo_config_file, "--start", "--step-length", "1.0", "--no-warnings"])
        
    def __init_users(self, users, time_step_seconds):
        
        self.output([f"\n[Simulator] Init users\n"])
        
        for i, user in enumerate(users) :
            
            self.output([f"[{i+1}/{len(users)}] user {user.id}:\n"])
            
            self.init_user(user, time_step_seconds)
                
    def __del__(self):
        
        if traci.isLoaded():
            traci.close()
            
    ###### REGIONS #######

    def get_region_user_XY(self, x, y):

        x_idx = (x - self.xmin) // self.regions_size
        y_idx = (y - self.ymin) // self.regions_size
    
        num_cols = (self.xmax - self.xmin) // self.regions_size
        region_id = y_idx * num_cols + x_idx
        
        if region_id < len(self.regions) and region_id >= 0:
            return int(region_id)
        else:
            return None

    def get_number_regions(self):
        
        return self.regions.size
            
    def get_adjacent_regions(self, regions, row, col):

        adjacent_regions = []

        for dr in [-1, 0, 1]:
            for dc in [-1, 0, 1]:
                if dr == 0 and dc == 0:
                    continue  
                new_row, new_col = row + dr, col + dc
                if 0 <= new_row < regions.shape[0] and 0 <= new_col < regions.shape[1]:
                    adjacent_regions.append((new_row, new_col))

        return adjacent_regions
    
    ###### POSITIONS #######
    
    def coord_to_lane(self, x, y, radius=10):
        lanes = self.net.getNeighboringLanes(x, y, radius, includeJunctions=False)
  
        # pick the closest edge 
        if len(lanes) > 0:
            distancesAndLanes = sorted([(dist, lane) for lane, dist in lanes], key=lambda x: (x[0], x[1].getID()))
            dist, closestLane = distancesAndLanes[0]
            return dist, closestLane
        
        else:
            return None
        
    def coord_to_edge(self, x, y, radius=10):
        
        edges = self.net.getNeighboringEdges(x, y, radius, includeJunctions=False)
        
        # pick the closest edge 
        if len(edges) > 0:
            distancesAndEdges = sorted([(dist, edge) for edge, dist in edges], key=lambda x: (x[0], x[1].getID()))
            dist, closestEdge = distancesAndEdges[0]
            return dist, closestEdge
        
        else:
            return None
        
    def get_gps_position(self, particle):
        
        x, y = self.get_cartesian_position(particle)
        return traci.simulation.convertGeo(x, y)

    def get_cartesian_position(self, particle):
    
        return traci.person.getPosition(particle.id)
       
    def convert_XY_to_LonLat(self, x, y):
                                           
        return self.net.convertXY2LonLat(x, y)
                                           
    def convert_LonLat_to_XY(self, lon, lat):
        
        return self.net.convertLonLat2XY(lon, lat) 
    
    def get_nb_people_in_region(self, particles, region_size):
        
        copy_particles = copy.deepcopy(particles)
        n_rows = (self.ymax - self.ymin) // self.regions_size + 1
        n_cols = (self.xmax - self.xmin) // self.regions_size + 1

        nb_people_in_region = np.zeros((n_rows, n_cols))
              
        for particle in copy_particles:
                            
            x, y = self.get_cartesian_position(particle)
            x_idx = int((x - self.xmin) // self.regions_size)
            y_idx = int((y - self.ymin) // self.regions_size)
        
            nb_people_in_region[y_idx][x_idx] += 1
            
        return nb_people_in_region

    def get_user_edge(self, user):
        # the vehicle is not departed yet
        if user.id not in traci.simulation.getDepartedPersonIDList() :

            #edge_id  = traci.person.getEdges(user.id)[0] # we take its insertion edge
            edge_id  = user.insertion_edge 
            position = 0

        else:

            lane_id  = traci.person.getLaneID(user.id)
            edge_id  = traci.lane.getEdgeID(lane_id)

            position = traci.lane.getLength(lane_id) - traci.person.getLanePosition(user.id)
        
        return edge_id, position

    def get_edge_XY_near_user(self, edge, user):

        # as we can not get X Y for an edge, we take the nearest edge's point from the user

        shapes = edge.getShape()

        x, y     = traci.person.getPosition(user.id)
        user_pos = [(x, y)]

        array1     = np.array(shapes)
        array2     = np.array(user_pos)
        distances  = cdist(array1, array2, metric='euclidean')
        idx        = np.argmin(distances)

        return shapes[idx]

    def update_state(self, particle):

        if traci.person.getStage(particle.id).type == 1 : # waiting 
            if particle.destination == Destinations.STATION:
                particle.destination = Destinations.STAY_AT_STATION
            else: 
                particle.destination = Destinations.STAY

    # abstract
    def set_new_destinations(self, particle, time_step_seconds=30):

        traci.person.removeStages(particle.id)

        if particle.destination == Destinations.STAY_AT_STATION:
                
            traci.person.appendWaitingStage(particle.id, time_step_seconds+1)
            
        elif particle.destination == Destinations.STAY: 
                
            traci.person.appendWaitingStage(particle.id, time_step_seconds+1)

        elif particle.destination == Destinations.POI: 

            user_edge_id, user_position = self.get_user_edge(particle)

            def length(from_edge, poi, vType):
                
                lane    = traci.poi.getParameter(poi, 'lane')
                to_edge = traci.lane.getEdgeID(lane)
                route   = traci.simulation.findRoute(from_edge, to_edge, vType=vType)
                
                return route, route.length 
                
            pois = traci.poi.getIDList()

            p_id = pois[0]
            route_min, min = length(user_edge_id, pois[0], 'my_pedestrian')

            for p in pois[1:]:

                if p != particle.last_poi:
                
                    temp_route, temp_min = length(user_edge_id, p, 'my_pedestrian')
                    
                    if temp_min < min and temp_min != 0:
                        min       = temp_min
                        route_min = temp_route
                        p_id      = p

            if route_min.edges:
                particle.last_poi = p_id
                poi_position = traci.poi.getParameter(p_id, 'pos')
                traci.person.appendWalkingStage(particle.id, route_min.edges, poi_position)

            traci.person.appendWaitingStage(particle.id, time_step_seconds+1)

        elif particle.destination == Destinations.STATION:        

            user_edge_id, user_position = self.get_user_edge(particle)

            def length(from_edge, busstop, vType):
                
                lane    = traci.busstop.getLaneID(busstop)
                to_edge = traci.lane.getEdgeID(lane)
                route   = traci.simulation.findRoute(from_edge, to_edge, vType=vType)
                
                return route, route.length
                
            bus_stops = traci.busstop.getIDList()
            
            b_id = bus_stops[0]
            route_min, min = length(user_edge_id, bus_stops[0], 'my_pedestrian')

            for b in bus_stops[1:]:

                if b != particle.last_bus: 
                    temp_route, temp_min = length(user_edge_id, b, 'my_pedestrian')
                    
                    if temp_min < min and temp_min != 0:
                        min       = temp_min
                        route_min = temp_route
                        b_id      = b

            if route_min.edges:
                particle.last_bus = b_id
                bus_position = traci.busstop.getStartPos(b_id)
                traci.person.appendWalkingStage(particle.id, route_min.edges, bus_position)

            traci.person.appendWaitingStage(particle.id, time_step_seconds+1)

        else:
                
            # ERROR
            pass

    def reload_users(self, old_users, new_users, time_step_seconds):            

        self.output([f"\n[Simulator] Reload users\n"])

        # remove old users
        for user in old_users:
            self.remove_user(user)
        
        for i, user in enumerate(new_users):
            
            self.output([f"[{i+1}/{len(new_users)}] user {user.id}:\n"])

            self.init_user(user, time_step_seconds)
                
    ###### SIMULATION #######

    def move_userXY(self, user, x, y, time_step_seconds, threshold=100):

        dist_and_closestEdge = self.coord_to_edge(x, y, threshold)

        if dist_and_closestEdge:

            dist, closestEdge = dist_and_closestEdge
            traci.person.moveToXY(user.id, closestEdge, x, y, matchThreshold=threshold, keepRoute=0)
            traci.person.appendWaitingStage(user.id, time_step_seconds+1)

        else:
                raise RuntimeError(f'No edge to move to for coordinates ({x},{y})')

    def remove_user(self, user):

        traci.person.remove(user.id)
    
    def init_user(self, user, time_step_seconds, threshold=50):

        lon, lat = user.position
        x, y     = self.net.convertLonLat2XY(lon, lat)

        dist_and_closestEdge = self.coord_to_edge(x, y, threshold)
        if dist_and_closestEdge:
            
            dist, closestEdge   = dist_and_closestEdge    
            user.insertion_edge = closestEdge.getID()

            if user.vehicle_type :

                traci.person.add(user.id, closestEdge.getID(), 0, typeID=user.vehicle_type)

            else:

                traci.person.add(user.id, closestEdge.getID(), 0)


            self.output([f"    Add user {user.id} on edge {closestEdge.getID()}.\n"])
            
            traci.person.appendWaitingStage(user.id, time_step_seconds+1)
            user.loaded = True

        else: 
            
            user.loaded = False

            self.output([f"    No close edge to insert this user.\n"])
    
    def simulation_step(self, time_step=1):
        for i in range(time_step):
            traci.simulationStep()
  
    def output(self, lines):

        if self.output_file:
            with open(self.output_file, "a+") as f:
                for l in lines:
                    f.write(l)