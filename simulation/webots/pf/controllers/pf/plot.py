import copy
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

def plot_density(density_list, save_file=None):

    density = copy.deepcopy(density_list)
    sum_density = np.sum(density)

    sns.set(style="whitegrid")
    sns.heatmap(density, cmap="Reds", square=False, vmax=sum_density, xticklabels=False, yticklabels=False)

    if save_file:
        plt.savefig(save_file)
        plt.clf()
    else:
        plt.show()
    
