import random
from enum import Enum

# abstract
Transitions  = Enum('Transitions', ['POI|STAY', 'STATION|STAY', 'POI|ANY', 'POI|STAY_AT_STATION', 'STATION|STAY_AT_STATION'])
# abstract
Destinations = Enum('Destinations', ['STAY_AT_STATION', 'STAY', 'STATION', 'POI'])

# abstract
class TransitionModel:
    
    def get_new_destination(self, position, transition_probabilities):

        if position == Destinations.STAY_AT_STATION:
                        
            if random.random() < transition_probabilities[Transitions['STATION|STAY_AT_STATION'].value-1]:
                    
                return Destinations.STATION   
                            
            elif random.random() < transition_probabilities[Transitions['POI|STAY_AT_STATION'].value-1]:
                        
                return Destinations.POI
                    
            else : 
                
                return Destinations.STAY_AT_STATION
                        
        elif position == Destinations.STAY:

            if random.random() < transition_probabilities[Transitions['POI|STAY'].value-1]:
                        
                return Destinations.POI
                    
            elif random.random() < transition_probabilities[Transitions['STATION|STAY'].value-1]:
                        
                return Destinations.STATION
                
            else:
                    
                return position
                    
        else:

            if random.random() < transition_probabilities[Transitions['POI|ANY'].value-1]:

                return Destinations.POI
                    
            else:

                return position 