from TransitionModel import Destinations
import random

# abstract
class Particle:
    
    def __init__(self, id, transition_probabilities_model, weight, position, home, departure_time, vehicle_type=None):

        self.id                       = id
        self.origin_id                = id # the id in the user data

        self.home                     = home
        self.loaded                   = False
        self.weight                   = weight
        self.position                 = position # gps coordinates (lon, lat)
        self.last_poi                 = None 
        self.last_bus                 = None 
        self.destination              = Destinations.STAY
        self.vehicle_type             = vehicle_type
        self.departure_time           = departure_time
        self.insertion_edge           = None
        self.transition_probabilities = []


        self.__init_transition_probabilities(transition_probabilities_model)
    
    def __init_transition_probabilities(self, transition_probabilities_model):
        
        for proba in transition_probabilities_model:

            self.transition_probabilities.append(random.choice(proba))
            
    def __str__(self):
        return f"Particle {self.id} : [home : {self.home}, weight : {self.weight}, position : {self.position}, " \
               f"destination : {self.destination}, vehicle_type : {self.vehicle_type}, " \
               f"departure_time : {self.departure_time}, transition proba : {self.transition_probabilities}]"