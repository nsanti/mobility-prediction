import numpy as np
import random
from pyemd import emd, emd_with_flow
from scipy.spatial.distance import euclidean
import copy 

from Particle import Particle
import HumanMobilitySimulator as sim 

class ParticleFilter:
    
    def __init__(self, region_size,\
                obs_confidence, behavorial_noise, position_noise, operator_diffusion_ratio, \
                user_allowance_ratio, probability_observation_nearby_people, \
                transition_probabilities_model, \
                seed, output_dir=None):
        
        self.particles = []

        self.regions_size = region_size

        #self.time_step_seconds    = time_step_seconds 

        self.obs_confidence   = obs_confidence
        self.position_noise   = position_noise 
        self.behavorial_noise = behavorial_noise 

        self.user_allowance_ratio                  = user_allowance_ratio    
        self.operator_diffusion_ratio              = operator_diffusion_ratio 
        self.probability_observation_nearby_people = probability_observation_nearby_people   

        self.transition_probabilities_model = transition_probabilities_model

        #self.time_step  = 1  
        self.delta_time = 0

        self.output_dir = output_dir 

        self.generator = np.random.default_rng(seed)

    def init(self, users, pois, stations, speed, width, height):
        # [weight, state]
        # state = [(lat,long), [transition proba]]
        
        weight = 1/len(users)

        self.map = sim.Map(width, height)
        
        for u in users:
            
            transitions = []
            for proba in self.transition_probabilities_model:
                transitions.append(random.choice(proba))

            # user = id, start position (x,y), home (x,y)
            home = sim.PointOfInterest(u[2][0], u[2][1], f'home {u[0]}')
            person = sim.Person(u[1][0], u[1][1], u[0], speed, transitions, home)
            particle = Particle(u[0], person, weight)
            self.particles.append(particle)
            self.map.add_person(person)
        
        for p in pois:
            
            p = sim.PointOfInterest(p[1], p[2], f'poi {p[0]}')
            self.map.add_poi(p)

        for s in stations:
            
            s = sim.PointOfInterest(s[1], s[2], f'station {s[0]}')
            self.map.add_station(s)

        num_regions_x = self.map.width // self.regions_size
        num_regions_y = self.map.height // self.regions_size

        # Calculate the centroids of each region
        centroids = []
        for i in range(num_regions_y):
            for j in range(num_regions_x):
                x = j * self.regions_size + self.regions_size / 2
                y = i * self.regions_size + self.regions_size / 2
                centroids.append((x, y))

        # Create the distance matrix based on the centroids
        self.distance_matrix = np.zeros((len(centroids), len(centroids)))

        for i in range(len(centroids)):
            for j in range(len(centroids)):
                self.distance_matrix[i, j] = euclidean(centroids[i], centroids[j])

    def compute_likelihood(self, particle, obs_density, k, u, l):

        likelihood  = 1.0  # initialize likelihood
        
        x, y = particle.person.x, particle.person.y 

        region_id = self.map.get_region_id(x, y, self.regions_size)
        nb_people_region = obs_density.get(region_id, 0)
        correct_obs = self.generator.binomial(nb_people_region, k * u * l)

        adj_regions = self.map.get_adjacent_regions(region_id, obs_density)
        
        for r in adj_regions :
    
            nb_people_adj_region = obs_density[r]
            wrong_obs = self.generator.binomial(nb_people_adj_region, k * u * (1 - l))

            self.output([f'    - likelihood formula : {likelihood} * ({k} * {u} * ({correct_obs} + {wrong_obs}))\n'])

            likelihood *= (k * u * (correct_obs + wrong_obs))
        
        return likelihood
            
    def resample(self):

        total_weight = sum(particle.weight for particle in self.particles)
        num_sample = len(self.particles)
        resampled_particles = []

        for _ in range(num_sample):
            pointer = random.uniform(0, total_weight)

            cumulative_weight = 0
            for particle in self.particles:
                cumulative_weight += particle.weight
                if pointer <= cumulative_weight:
                    new_sample = copy.deepcopy(particle)
                    new_sample.weight = 1/num_sample
                    resampled_particles.append(new_sample)
                    break

        for i, p in enumerate(resampled_particles):
            #self.particles[i].person.y = p.person.y
            #self.particles[i].person.x = p.person.x
            self.particles[i].weight = p.weight
            self.particles[i].person.transitions = p.person.transitions
            self.particles[i].destination = p.person.destination
            self.particles[i].home = p.person.home
            self.particles[i].state = p.person.state

    def normalize_weights(self):
            
            sum_weights = 0.0

            for sample in self.particles:
                sum_weights += sample.weight 
            
            if sum_weights < 1e-15:

                self.output([f"Weight normalization failed, sum of weights: {sum_weights} (weights will be renitialized)\n"])

                for sample in self.particles:
                    sample.weight = 1 / len(self.particles)

            else :

                self.output([f"Weight normalization, sum of weights: {sum_weights}\n"])

                for sample in self.particles:
                    sample.weight /= sum_weights 
            
    def output(self, lines):

        if self.output_dir:
            with open(self.output_dir + '/output.txt', "a+") as f:
                for l in lines:
                    f.write(l)

    def blind_step(self):

        self.map.update()
        for _, p in self.map.people.items():
            p.x = min(p.x + self.position_noise, self.map.width-1) # if the position + position noise is not in the map boundary
            p.y = min(p.y + self.position_noise, self.map.height-1)
        self.delta_time += 1 
    
    def res(self, obs_data, time, add_name=''):

        sim_data = self.map.calculate_density(self.regions_size)
        self.output([f"sim data {self.delta_time} : {sim_data}\n"]);
        self.output([f"obs data {self.delta_time} : {obs_data}\n"]);

        self.map.visualize_density(self.regions_size, sim_data, file_name=f'{self.output_dir}/sim_res_{time}_{add_name}')
        self.map.visualize_density(self.regions_size, obs_data, file_name=f'{self.output_dir}/obs_res_{time}_{add_name}')

    def observation_step(self, obs_data):

        self.output([f"Observation provided, update:\n"])  
        sim_data = self.map.calculate_density(self.regions_size)

        sim_data_arr = self.map.density_to_array(self.regions_size, sim_data)
        obs_data_arr = self.map.density_to_array(self.regions_size, obs_data)

        first_histogram  = np.array(sim_data_arr, dtype=np.float64).reshape(-1)
        second_histogram = np.array(obs_data_arr, dtype=np.float64).reshape(-1)

        _, flow = emd_with_flow(first_histogram, second_histogram, self.distance_matrix)

        num_regions_x = self.map.width // self.regions_size

        for i, particle in enumerate(self.particles):

            self.output([f"Particle {particle.id}:\n"])
            x, y = particle.person.x, particle.person.y 
            x_o = x // self.regions_size
            y_o = y // self.regions_size
            origin = y_o + x_o * num_regions_x
            origin = int(origin) 
            self.output([f"    - origin particle: {origin}\n"])

            try:
                #destination = next((j for j, d in enumerate(flow[origin]) if j != origin and d > 0), None) # search the first edm flow
                destination = next((j for j, d in enumerate(flow[origin]) if d > 0), None) # search the first edm flow
            except IndexError:
                print(f"error, destination : {destination}, origin : {origin}, (x_o,y_o) : ({x_o},{y_o}), (x,y): ({x},{y}), num_regions_x: {num_regions_x}")
                destination = 100

            if destination is not None:

                    self.output([f"    - approach particle {particle.id} from region {origin} to region {destination}\n"])
                    
                    region_x = destination // num_regions_x  # calculate the x-coordinate of the destination region
                    region_y = destination % num_regions_x # calculate the y-coordinate of the destination region
                    x_d = region_x * self.regions_size + self.regions_size / 2
                    y_d = region_y * self.regions_size + self.regions_size / 2

                    delta_x = self.obs_confidence * (x_d - x)
                    delta_y = self.obs_confidence * (y_d - y)

                    particle.person.x += delta_x 
                    particle.person.y += delta_y 

                    flow[origin][destination] -= 1
            
            self.output([f"    - old transition proba: {particle.transition_probabilities}\n"])

            particle.person.transitions = list(map(lambda x: x + self.behavorial_noise, \
                                                    particle.person.transitions))
            self.output([f"    - new transition proba: {particle.person.transitions}\n"])
            
            particle.likelihood = self.compute_likelihood(particle, obs_data, \
                                                      self.operator_diffusion_ratio, self.user_allowance_ratio, \
                                                      self.probability_observation_nearby_people) 
            self.output([f"    - likelihood : {particle.likelihood}\n"])

            particle.weight = particle.likelihood
            self.output([f"    - weight : {particle.weight}\n"])
        
        #self.normalize_weights() 
        #self.resample() 

        #self.map.clean_people()
        #for p in self.particles:
        #    self.map.add_person(p.person)

        self.delta_time += 1 

    def check(self):
        homes = []
        dest = []
        state = []
        for _, person in self.map.people.items():
            homes.append(person.home)
            dest.append(person.destination)
            state.append(person.state)

        print('homes :', len(set(homes)))
        most_home = max(set(homes), key=homes.count)
        print('most home :', most_home) 

        print('dest:', len(set(dest)))
        most_dest = max(set(dest), key=dest.count)
        print('most dest:', most_dest) 

        print('state:', len(set(state)))