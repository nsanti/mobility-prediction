import numpy as np
import pandas as pd
from datetime import datetime
import ast

# abstract
class UsersData:
    
    def __init__(self, data_dir):
        
        self.data_dir   = data_dir 
        self.df_users   = []
        self.df_density = []
        self.start_time = None

        self.__init_users()
        self.__init_density()
        
    def __init_users(self):
        
        self.df_users = pd.read_csv(self.data_dir + 'data_individuals.csv')
        self.df_users['time'] = pd.to_datetime(self.df_users['time'])

        self.df_users = self.df_users.sort_values(by=['time'])

    def __init_density(self):

        self.df_density = pd.read_csv(self.data_dir + 'data_density.csv')
        self.df_density['time'] = pd.to_datetime(self.df_density['time'])
        self.df_density['density'] = self.df_density['density'].apply(ast.literal_eval)
        self.df_density = self.df_density.sort_values(by=['time'])
        self.start_time = self.df_density.iloc[0].time

    def get_users(self):
    
        df_start = pd.read_csv(self.data_dir + 'start.csv', dtype={'user':int})
        users = []

        for _, row in df_start.iterrows():
            # user = id, start position (x,y), home (x,y)
            users.append([row.user, (row.start_x, row.start_y), (row.home_x, row.home_y)])

        return users
    
    def get_pois(self):

        df_poi = pd.read_csv(self.data_dir + 'poi.csv')
        pois = []

        for _, row in df_poi.iterrows():
            pois.append([row.poi, row.x, row.y])
        
        return pois

    def get_stations(self):

        df_stations = pd.read_csv(self.data_dir + 'stations.csv')
        stations = []

        for _, row in df_stations.iterrows():
            stations.append([row.station, row.x, row.y])
        
        return stations 

    def get_nb_people_regions(self, delta_time, time_window=30):
        
        start_time = self.start_time + pd.Timedelta(seconds=delta_time)
        end_time   = start_time + pd.Timedelta(seconds=time_window)

        filtered = self.df_density.loc[(self.df_density['time'] >= start_time) & (self.df_density['time'] <= end_time)]
        min_time = filtered['time'].min()
        min_df = self.df_density[self.df_density['time'] == min_time]
        
        return min_df.iloc[0].density 