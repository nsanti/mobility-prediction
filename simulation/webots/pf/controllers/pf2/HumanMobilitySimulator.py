import matplotlib.pyplot as plt
from enum import Enum
import random
import seaborn as sns
from datetime import datetime
import pandas as pd
import os

State = Enum('State', ['STAY', 'MOVING', 'MOVING_TO_STATION', 'STAY_AT_STATION'])
Transition = Enum('Transition', ['HOME|STAY', 'STATION|STAY', 'POI|ANY', 'HOME|STAY_AT_STATION'])
Destination = Enum('Destination', ['POI', 'STAY', 'HOME', 'STATION'])

def transition(person):
    
   if person.state == State.STAY_AT_STATION:
        if random.random() < person.transitions[Transition['HOME|STAY_AT_STATION'].value-1]:
            return Destination.HOME
        else:
            return Destination.STAY
   else:
        if random.random() < person.transitions[Transition['STATION|STAY'].value-1]:
            return Destination.STATION
        elif random.random() < person.transitions[Transition['HOME|STAY'].value-1]:
            return Destination.HOME
        elif random.random() < person.transitions[Transition['POI|ANY'].value-1]:
            return Destination.POI
        else:
            return Destination.STAY 

class Person:
    def __init__(self, x, y, id, speed, transitions, home):
        self.id = id
        self.x = x
        self.y = y
        self.home = home
        self.speed = speed
        self.destination = None
        self.state = State.STAY
        self.transitions = transitions 

    def __str__(self) -> str:
        if self.destination:
            dest = (self.destination.x, self.destination.y)
        else:
            dest = None
        return f"Person {self.id}, coord ({self.x}, {self.y}), home ({self.home.x}, {self.home.y}), dest {dest}, state {self.state}"
        
    def move(self):
        if (self.state == State.MOVING or self.state == State.MOVING_TO_STATION) and self.destination is not None:
            dx = self.destination.x - self.x
            dy = self.destination.y - self.y
            distance = (dx ** 2 + dy ** 2) ** 0.5
            if distance > self.speed:
                self.x += self.speed * dx / distance
                self.y += self.speed * dy / distance
            else:
                self.x, self.y = self.destination.x, self.destination.y
                self.wait()
        
    def set_destination(self, destination, station=False):
        self.destination = destination
        if station:
            self.state = State.MOVING_TO_STATION
        else:
            self.state = State.MOVING

    def set_transitions(self, transitions):
        self.transitions = transitions

    def wait(self):
        self.destination = None

        if self.state == State.MOVING_TO_STATION:
            self.state = State.STAY_AT_STATION
        else:
            self.state = State.STAY

class PointOfInterest:
    def __init__(self, x, y, name):
        self.x = x
        self.y = y
        self.name = name

    def __str__(self) -> str:
        return f"Poi name : {self.name}, coord ({self.x}, {self.y})"

class Map:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.poi = []
        self.stations = []
        self.people = dict()

    def add_poi(self, poi):
        self.poi.append(poi)

    def add_station(self, station):
        self.stations.append(station)

    def add_person(self, person):
        self.people[person.id] = person

    def remove_person(self, person):
        self.people.pop(person.id, None)
    
    def clean_people(self):
        self.people = dict()

    def update(self):
        for _, person in self.people.items():
            if person.state == State.STAY or person.state == State.STAY_AT_STATION:
                destination = transition(person)
                self.change_people_dest(person, destination)
            person.move()

    def visualize(self, file_name=None):
        plt.clf()
        plt.figure()
        plt.xlim(0, self.width)
        plt.ylim(0, self.height)
        for poi in self.poi:
            plt.plot(poi.x, poi.y, 'x', label=poi.name)
        for id, person in self.people.items():
            plt.plot(person.x, person.y, 'o')
        plt.grid(True)

        if file_name:
            plt.savefig(file_name)
        else:
            plt.show()

    def calculate_nearest_poi(self, person, threshold=30):
        nearest_poi = None
        min_distance = float('inf')
        for poi in self.poi:
            if poi != person.destination:
                dx = poi.x - person.x
                dy = poi.y - person.y
                distance = (dx ** 2 + dy ** 2) ** 0.5
                if distance < min_distance and distance > threshold:
                    min_distance = distance
                    nearest_poi = poi
        return nearest_poi
    
    def calculate_nearest_station(self, person, threshold=30):
        nearest_station = None
        min_distance = float('inf')
        for station in self.stations:
            if station != person.destination:
                dx = station.x - person.x
                dy = station.y - person.y
                distance = (dx ** 2 + dy ** 2) ** 0.5
                if distance < min_distance and distance > threshold:
                    min_distance = distance
                    nearest_station = station
        return nearest_station
    
    def change_people_dest(self, person, destination):
       if destination == Destination.STAY:
            person.wait()
       elif destination == Destination.POI:
            poi = self.calculate_nearest_poi(person)
            person.set_destination(poi)
       elif destination == Destination.STATION:
            station = self.calculate_nearest_station(person)
            person.set_destination(station, station=True)
       elif destination == Destination.HOME:
            person.set_destination(person.home) 
    
    def get_region_id(self, x, y, region_size):
        region_x = x // region_size  
        region_y = y // region_size  
        region = (region_x, region_y)

        return region
    
    def get_adjacent_regions(self, region_id, density_dict):
        adjacent_regions = []
        (region_x, region_y) = region_id

        relative_positions = [(1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (-1, -1), (1, -1), (-1, 1)]

        for dx, dy in relative_positions:
            adjacent_region = (region_x + dx, region_y + dy)
            if adjacent_region in density_dict:
                adjacent_regions.append(adjacent_region)

        return adjacent_regions

    def calculate_density(self, region_size, to_array=False):
        density = dict() 

        for id, p in self.people.items():
            x, y = p.x, p.y
            region_x = x // region_size  
            region_y = y // region_size  
            region = (region_x, region_y) 
    
            density[region] = density.get(region, 0) + 1

        if to_array:
            density = [[density.get((i, j), 0) for j in range(self.width // region_size)] for i in range(self.height // region_size)]

        return density

    def density_to_array(self, region_size, density):
            return [[density.get((i, j), 0) for j in range(self.width // region_size)] for i in range(self.height // region_size)]
    
    def visualize_density(self, region_size, density, file_name=None):
        plt.clf()
        density_plot = [[density.get((i, j), 0) for j in range(self.width // region_size)] for i in range(self.height // region_size)]

        sns.set(style="whitegrid")
        sns.heatmap(density_plot, cmap="Reds", square=False, xticklabels=False, yticklabels=False)

        if file_name:
            plt.savefig(file_name)
        else:
            plt.show()