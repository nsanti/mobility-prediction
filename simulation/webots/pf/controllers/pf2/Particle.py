import random

# abstract
class Particle:
    
    def __init__(self, id, person, weight):

        self.id                       = id
        self.weight                   = weight
        self.person                   = person 
        self.likelihood               = None     
        self.edm_destination          = None
        self.transition_probabilities = []

    def __str__(self):
        return f"Particle {self.id} : weight : {self.weight}"