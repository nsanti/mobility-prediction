"""pf2 controller."""

import os
import ast
import sys
import time 
import struct

from UsersData import UsersData
from ParticleFilter import ParticleFilter
from controller import Robot, Receiver, Emitter

# robot init
robot = Robot()
#timestep = int(robot.getBasicTimeStep())

# get args
robot_id = sys.argv[1]
skip_obs = ast.literal_eval(sys.argv[2]) # minutes

# parse args
if len(skip_obs) != 2:
    skip_obs = None
else:
    skip_obs[0] *= 60 # to seconds
    skip_obs[1] *= 60 # to seconds


# seed for synchronisation of the random generator
seed = 1688813763

# pf function init
output_dir = f'../../results/exp_20_10-20/robot_{robot_id}/'
output_file = output_dir + 'output.txt'

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

if os.path.isfile(output_file):
    os.remove(output_file)
open(output_file, 'w').close()

users_data = UsersData('data/1500_60/')           

timestep = 1000 # milliseconds (1000ms = 1s)
obs_time = 10 # seconds 
res_time = 30 # seconds
end_time = 60 # minutes 
end_time *= 60
obs_time /= (timestep/1000)
res_time /= (timestep/1000)

width = 1000  
height = 1000

speed                                 = 1.42 # meters/second
region_size                           = 100
obs_confidence                        = 0.8 
position_noise                        = 0.1
behavorial_noise                      = 0.01
user_allowance_ratio                  = 1 # y
operator_diffusion_ratio              = 1 # k  
probability_observation_nearby_people = 0.2 # λ

transition_probalities_model = [[0.05, 0.1, 0.15, 0.2], [0, 0.05, 0.1, 0.15], [0.05, 0.1], [0, 0.1, 0.2, 0.3]] 

pf = ParticleFilter(
                    region_size, obs_confidence, behavorial_noise, position_noise, \
                    operator_diffusion_ratio, user_allowance_ratio, probability_observation_nearby_people, \
                    transition_probalities_model, \
                    seed, output_dir
                   )

pf.init(users_data.get_users(), users_data.get_pois(), users_data.get_stations(), speed, width, height)

obs_step = 0
res_step = 0

while robot.step(timestep) != -1:

    now = int(robot.getTime())

    if now >= end_time:
        break  

    pf.blind_step()

    if obs_step == obs_time:
        if not skip_obs or (now >= skip_obs[0] and now < skip_obs[1]):
            obs_data = users_data.get_nb_people_regions(now)
            pf.res(obs_data, now, 'before')
            pf.observation_step(obs_data)
            pf.res(obs_data, now, 'after')
            obs_step = 0
    
    #if res_step == res_time :
    #    obs_data = users_data.get_nb_people_regions(now)
    #    pf.res(obs_data, now)
    #    res_step = 0

    obs_step +=1
    res_step +=1

sys.exit(0)

